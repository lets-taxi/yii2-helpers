<?php

namespace ltcorp\helpers;

use Yii;

use app\models\UserSession;

/**
 * Class HUserSession
 * @package ltcorp\helpers
 */
class HUserSession
{
    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRED = 9;

    /**
     * @param $userId
     * @return UserSession
     */
    public static function openNewSession($userId)
    {
        self::closeActiveSession($userId);

        $session = new UserSession();
        $session->user_id = $userId;
        $session->ip = getenv('REMOTE_ADDR');
        $session->auth_key = Yii::$app->security->generateRandomString();
        $session->status = 1;
        $session->save();

        return $session;
    }

    /**
     * @param $userId
     */
    public static function closeActiveSession($userId) //TODO: move to model ...
    {
        UserSession::updateAll(['expired_at' => date('Y-m-d H:i:s'), 'status' => self::STATUS_EXPIRED], ['user_id' => $userId, 'status' => self::STATUS_ACTIVE]);
    }

}
