<?php

namespace ltcorp\helpers;

use Yii;
use yii\helpers\FileHelper;

/**
 * Class HSystemLog
 * @package ltcorp\helpers
 */
class HSystemLog
{
    const TYPE_INFO = 1;
    const TYPE_WARNING = 5;
    const TYPE_ERROR = 9;

    private $file = '';
    private $onlyDev;

    private $logTemplate = "%s  [%s]  %s \r\n"; //"date  [type]  message"


    /**
     * @param $category
     * @param $file
     * @param bool|true $onlyDev
     */
    public function __construct($category, $file, $onlyDev = true)
    {
        $path = Yii::$app->runtimePath . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $category;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }
        $this->file = $path . DIRECTORY_SEPARATOR . $file;

        $this->onlyDev = $onlyDev;
        return true;
    }

    /**
     * @param $message
     * @param int $type
     * @return bool
     */
    public function write($message, $type = self::TYPE_INFO)
    {
        if ($this->onlyDev && !YII_DEBUG) {
            return false;
        }

        return error_log(
            sprintf($this->logTemplate, date('d/m/Y H:i:s'), $this->getMessageType($type), $message),
            3,
            $this->file
        );
    }

    /**
     * @param $typeId
     * @return mixed
     */
    private function getMessageType($typeId)
    {
        return [
            self::TYPE_INFO => 'info',
            self::TYPE_WARNING => 'warning',
            self::TYPE_ERROR => 'error',
        ][$typeId];
    }
}