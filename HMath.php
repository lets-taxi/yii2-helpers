<?php

namespace ltcorp\helpers;

/**
 * Class HMath
 * @package ltcorp\helpers
 */
class HMath
{
    /**
     * Take 2 numeric arrays with coordinates (points) and calculate vector coordinates
     *
     * @param array $a [x, y]
     * @param array $b [x, y]
     * @return array
     */
    public static function calculateVector(array $a, array $b)
    {
        return [$b[0] - $a[0], $b[1] - $a[1]];
    }

    /**
     * Take numeric array with coordinates (vector) and calculate angle between this vector and oX
     *
     * @param $v [x, y]
     * @return float
     */
    public static function calculateXVectorAngle($v)
    {
        return self::calculateVectorsAngle($v, [1, 0]);
    }

    /**
     * Take numeric array with coordinates (vector) and calculate angle between this vector and oY
     *
     * @param $v [x, y]
     * @return float
     */
    public static function calculateYVectorAngle($v)
    {
        return self::calculateVectorsAngle($v, [0, 1]);
    }

    /**
     * Take 2 numeric arrays with coordinates (vectors) and calculate angle between them
     *
     * @param $v1 [x, y]
     * @param $v2 [x, y]
     * @return float
     */
    public static function calculateVectorsAngle($v1, $v2)
    {
        $x1 = $v1[0];
        $y1 = $v1[1];
        $x2 = $v2[0];
        $y2 = $v2[1];

        $scalar = (($x1 * $x2) + ($y1 * $y2));
        $length1 = self::calculateVectorLength($v1);
        $length2 = self::calculateVectorLength($v2);
        //division by zero protection
        $angleCos = (($length1 * $length2)<>0) ? ($scalar / ($length1 * $length2)) : 0;

        $radians = acos($angleCos);

        return rad2deg($radians);
    }

    /**
     * @param $v [x, y]
     * @return float
     */
    public static function calculateVectorLength($v)
    {
        return sqrt(pow($v[0], 2) + pow($v[1], 2));
    }
}