<?php

namespace ltcorp\helpers;

use yii\helpers\Url;

/**
 * Class HCurlRest
 * @package ltcorp\helpers
 */
class HCurlRest extends HCurl
{
    const REQUEST_GET = 'GET';
    const REQUEST_POST = 'POST';
    const REQUEST_PUT = 'PUT';
    const REQUEST_DELETE = 'DELETE';

    protected $patterns = [
        'header_status_code' => '/HTTP\/1\.1 (\d{3}) /',
    ];

    protected $httpCodeGroups = [
        'ok' => [200, 302, 301, 429, 403],
    ];

    /**
     * @return array
     */
    protected function execute()
    {
        $body = null;
        $headers = null;
        $cookies = null;

        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        $response = curl_exec($this->curl);

        $header_size = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        $headerCode = $this->parse($this->patterns['header_status_code'], $header);

        return [
            'response_code' => $headerCode[1],
            'header' => $header,
            'body' => $body
        ];
    }

    /**
     * @param $method
     * @param $host
     * @param $request
     * @param array $params
     * @return array
     */
    public function sendRequest($method, $host, $request, $params = [])
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);

        switch ($method) {
            case self::REQUEST_GET:
            case self::REQUEST_DELETE:
                $link = Url::to([$request] + $params);
                break;
            case self::REQUEST_PUT:
            case self::REQUEST_POST:
            default:
                $link = Url::to([$request]);
                curl_setopt($this->curl, CURLOPT_POST, 1);
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($params));
                break;
        }

        $this->current_url = $host . $link;
        return $this->execute();
    }
}