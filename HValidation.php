<?php

namespace ltcorp\helpers;

/**
 * Class HValidation
 * @package ltcorp\helpers
 */
class HValidation
{
    /**
     * @param $value
     * @return int
     */
    public static function phone($value)
    {
        return preg_match('/^[\d\+\-\(\) ]{7,20}$/i', $value);
    }


}