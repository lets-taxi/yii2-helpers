<?php

namespace ltcorp\helpers;

/**
 * Class HHeatmap
 * @package ltcorp\helpers
 */
class HHeatmap
{
    const ZOOM_HOUSE = 16;
    const ZOOM_STREET = 13;
    const ZOOM_COMMUNITY = 10;
    const ZOOM_CITY = 9;

    const DECIMAL_HOUSE = 5;
    const DECIMAL_STREET = 4;
    const DECIMAL_COMMUNITY = 3;
    const DECIMAL_CITY = 2;

    /**
     * Get decimal to round coordinates for zoom
     * @param int $zoom
     * @return int
     */
    public static function getDecimalByZoom($zoom = 11)
    {
        if ($zoom >= self::ZOOM_HOUSE) {
            return self::DECIMAL_HOUSE;
        } else if ($zoom >= self::ZOOM_STREET) {
            return self::DECIMAL_STREET;
        } else if ($zoom >= self::ZOOM_COMMUNITY) {
            return self::DECIMAL_COMMUNITY;
        } else {
            return self::DECIMAL_CITY;
        }
    }

    /**
     * Get name columns table for zoom
     * @param int $zoom
     * @return array
     */
    public static function getColumnsByZoom($zoom = 11)
    {
        if ($zoom >= self::ZOOM_HOUSE) {
            return [
                'pickup_latitude' => 'pickup_latitude_house',
                'pickup_longitude' => 'pickup_longitude_house'
            ];
        } else if ($zoom >= self::ZOOM_STREET) {
            return [
                'pickup_latitude' => 'pickup_latitude_street',
                'pickup_longitude' => 'pickup_longitude_street'
            ];
        } else if ($zoom >= self::ZOOM_COMMUNITY) {
            return [
                'pickup_latitude' => 'pickup_latitude_community',
                'pickup_longitude' => 'pickup_longitude_community'
            ];
        } else {
            return [
                'pickup_latitude' => 'pickup_latitude_city',
                'pickup_longitude' => 'pickup_longitude_city'
            ];
        }
    }
}