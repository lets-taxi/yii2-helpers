<?php

namespace ltcorp\helpers;

/**
 * Class HGeo
 * @package ltcorp\helpers
 */
class HGeo
{
    protected static $metersInGrade = 111120; // 60 * 1852 | 1 grade = 60 sea miles

    /**
     * @param $gps
     * @param int $coef 1000 - km, 1 - meters etc
     * @return float
     */
    public static function simpleConvertGPSDistance2Meter($gps, $coef = 1000)
    {
        return ($gps * self::$metersInGrade) / $coef;
    }
}