<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.03.2016
 * Time: 17:45
 */

namespace ltcorp\helpers;

use Yii;

/**
 * Class HSecurity
 * @package ltcorp\helpers
 */
class HSecurity
{
    /**
     * @return array
     */
    public static function generateTokenResetPassword()
    {
        $token = [
            '1' => Yii::$app->security->generateRandomString(16),
            '2' => Yii::$app->security->generateRandomString(16),
            '3' => Yii::$app->security->generateRandomString(16),
        ];

        foreach ($token as $part => $value) {
            while (preg_match('/\-/u', $token[$part])) {
                $token[$part] = Yii::$app->security->generateRandomString(16);
            }
        }

        return [
            'string' => $token[3] . '-' . $token[1] . '-' . $token[2],
            'token-password' => $token[3] . '-' . $token[2],
            'token-confirm' => $token[1]
        ];
    }

    /**
     * @param $token
     * @return array|bool
     */
    public static function parserTokenResetPassword($token)
    {

        $parts = explode('-', $token);

        if (count($parts) != 3) {
            return false;
        }

        return [
            'string' => $token,
            'token-password' => $token[2] . '-' . $token[1],
            'token-confirm' => $parts[0]
        ];

    }

}