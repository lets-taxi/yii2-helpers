<?php

namespace ltcorp\helpers;

use Yii;
use app\models\Log;

/**
 * Class HLog
 * @package ltcorp\helpers
 */
class HLog
{
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_DELETE = 9;

    /**
     * @param $essenceName
     * @param $essenceId
     * @param $action
     * @param array $details
     * @return mixed
     */
    public static function write($essenceName, $essenceId, $action, array $details = [])
    {
        $log = new Log();
        $log->attributes = [
            'user_id' => Yii::$app->user->identity->getId(),
            'ip' => getenv('REMOTE_ADDR'),

            'essence_name' => $essenceName,
            'essence_id' => $essenceId,
            'action' => $action,
            'details' => serialize($details)
        ];

        return $log->save();
    }

    public static function revert($id)
    {
        //TODO revert for changed log by details ...
    }
}