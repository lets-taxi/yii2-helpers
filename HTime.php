<?php

namespace ltcorp\helpers;

use Yii;

/**
 * Class HTime
 * @package ltcorp\helpers
 */
class HTime
{
    const DEFAULT_TIMEZONE = 'Europe/Moscow';

    /**
     * @return \DateTime
     */
    public static function getCurrentDate()
    {
        return self::setLocalTimezone(new \DateTime());
    }

    /**
     * @param mixed $date
     * @return \DateTime
     */
    public static function setLocalTimezone($date)
    {
        if (!is_object($date) || get_class($date) != \DateTime::class) {
            $date = self::getDateTime($date);
        }
        if (!empty(Yii::$app->params['system']['timezone'])) {
            $timezone = Yii::$app->params['system']['timezone'];
        } else {
            $timezone = self::DEFAULT_TIMEZONE;
        }

        $date->setTimezone(new \DateTimeZone($timezone));
        return $date;
    }

    /**
     * @param null $date
     * @return \DateTime
     */
    public static function getDateWithLocalTimezone($date = null)
    {
        if (!empty(Yii::$app->params['system']['timezone'])) {
            $timezone = Yii::$app->params['system']['timezone'];
        } else {
            $timezone = self::DEFAULT_TIMEZONE;
        }
        return new \DateTime($date, new \DateTimeZone($timezone));
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return int
     */
    public static function getDateDiffInSeconds(\DateTime $start, \DateTime $end)
    {
        $start = strtotime($start->format('Y-m-d H:i:s'));
        $end = strtotime($end->format('Y-m-d H:i:s'));
        return $end - $start;
    }

    /**
     * @param $date
     * @return \DateTime
     */
    public static function setActualDate($date)
    {
        $receivedDate = HTime::setLocalTimezone(new \DateTime($date));
        $localDate = HTime::setLocalTimezone(new \DateTime());

        $diff = abs($localDate->getTimestamp() - $receivedDate->getTimestamp()) / 60;

        if ($receivedDate > $localDate || ($receivedDate < $localDate && $diff > 15)) {
            $receivedDate = $localDate;
        }

        return $receivedDate;
    }

    /**
     * Create DateTime from string or timestamp
     * @param $date mixed
     * @return \DateTime
     */
    public static function getDateTime($date)
    {
        if ($date != intval($date)) {
            $date = strtotime($date);
        } else {
            $date = intval($date);
        }
        $dateTime = new \DateTime();
        $dateTime->setTimestamp($date);
        return $dateTime;
    }
}