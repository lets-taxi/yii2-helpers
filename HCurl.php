<?php

namespace ltcorp\helpers;

use ltcorp\helpers\HSystemLog;
use Yii;
use yii\base\ErrorException;

class HCurl
{
    /**
     * @var bool
     */
    protected $is_fast = true;

    /**
     * @var resource
     */
    protected $curl;

    /**
     * @var array
     */
    protected $curlOptions = [];

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $urls = [];

    /**
     * @var array
     */
    protected $patterns = [];

    /**
     * @var array
     */
    protected $httpCodes = [];

    /**
     * @var HSystemLog
     */
    protected $logger;

    /**
     * @var array
     */
    protected $response = [];

    /**
     * @param string $logName
     */
    public function __construct($logName)
    {
        $this->logger = new HSystemLog('hcurl', $logName, true);

        $this->logger->write('start');

        $this->httpCodes = $this->httpCodes();
        $this->logger->write('http codes: ' . var_export($this->httpCodes, true));

        $this->curlOptions = $this->curlOptions();
        $this->logger->write('curl options: ' . var_export($this->curlOptions, true));

        $this->urls = $this->urls();
        $this->logger->write('urls: ' . var_export($this->urls, true));

        $this->patterns = $this->patterns();
        $this->logger->write('patterns: ' . var_export($this->patterns, true));

        $this->initCurl();
    }

    /**
     * @return array
     */
    protected function httpCodes()
    {
        return [
            'ok' => [200, 302, 301],
            'bad' => [404, 500]
        ];
    }

    /**
     * @return array
     */
    protected function curlOptions()
    {
        return [
            CURLOPT_COOKIESESSION => true,
            CURLOPT_COOKIEFILE => Yii::$app->basePath . '/runtime/cookiefile.txt',
            CURLOPT_COOKIEJAR => Yii::$app->basePath . '/runtime/cookiejar.txt',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_VERBOSE => true,
            CURLOPT_HEADER => true,
        ];
    }

    /**
     * @return array
     */
    protected function urls()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function patterns()
    {
        return [
            'header_status_code' => '/HTTP\/1\.1 (\d{3}) /',
            'cookie' => '/Set-Cookie:(?<cookie>\s{0,}.*)$/im'
        ];
    }

    /**
     *
     */
    protected function initCurl()
    {
        $this->curl = curl_init();
        foreach ($this->curlOptions as $key => $option) {
            curl_setopt($this->curl, $key, $option);
        }
    }

    /**
     * @param null $url
     * @return bool
     * @throws ErrorException
     */
    protected function execute($url = null)
    {
        $body = null;
        $headers = null;
        $cookies = null;

        if (!$url) {
            $url = $this->url;
        }
        $this->logger->write('execute url: ' . $url);
        curl_setopt($this->curl, CURLOPT_URL, $url);
        $this->wait(15, 60);

        $this->setResponse(curl_exec($this->curl), $url);
        return $this->checkHeader();
    }

    /**
     * @param $response
     * @param $url
     */
    protected function setResponse($response, $url)
    {
        $headerSize = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
        $cookies = $this->parse($this->patterns['cookie'], $response, true);

        $this->response = [
            'header' => substr($response, 0, $headerSize),
            'cookies' => $cookies[1],
            'body' => substr($response, $headerSize),
            'url' => $url
        ];
    }

    /**
     * @return bool
     * @throws ErrorException
     * @internal param $header
     */
    protected function checkHeader()
    {
        if (empty($this->response['header'])) {
            $this->logger->write('response header empty', HSystemLog::TYPE_ERROR);
            return false;
        }
        $header = $this->response['header'];
        $pm = $this->parse($this->patterns['header_status_code'], $header);
        $this->logger->write('response code: ' . $pm[1]);
        try {
            foreach ($this->httpCodes as $groupName => $httpCodes) {
                if (in_array($pm[1], $httpCodes)) {
                    return $this->{$this->getHandlerName($groupName)}();
                }
            }
        } catch (ErrorException $e) {
            $this->logger->write('Error: ' . var_export($e, true), HSystemLog::TYPE_ERROR);
            throw new ErrorException;
        }
        return false;
    }

    /**
     * @param $pattern
     * @param $content
     * @param bool|false $multi
     * @return mixed
     */
    protected function parse($pattern, $content, $multi = false)
    {
        $multi ? preg_match_all($pattern, $content, $pm) : preg_match($pattern, $content, $pm);
        return $pm; //TODO add check pm[0] & pm[1]
    }

    /**
     * @param $min
     * @param null $max
     */
    protected function wait($min, $max = null)
    {
        if (!$this->is_fast) {
            if (!$max) {
                $max = $min;
            }
            $sleepingTime = rand($min, $max);
            $this->logger->write('wait ' . $sleepingTime . ' seconds');
            sleep($sleepingTime);
        }
    }

    /**
     * @return string
     */
    protected function getUserAgent()
    {
        if (!empty(Yii::$app->params['curl']['user_agents'])) {
            $list = Yii::$app->params['curl']['user_agents'];
            return $list[rand(0, count($list) - 1)];
        }

        return "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 5.1; Trident/5.0)";
    }

    /**
     * @param string $groupName
     * @return string
     */
    protected function getHandlerName($groupName)
    {
        $handlerName = 'handle' . ucfirst($groupName) . 'Code';
        if (method_exists($this, $handlerName)) {
            return $handlerName;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function handleOkCode()
    {
        return true;
    }

    /**
     * @return bool
     */
    protected function handleBadCode()
    {
        return false;
    }

}
