<?php

namespace ltcorp\helpers;

/**
 * Class HFile
 * @package ltcorp\helpers
 */
class HFile
{
    /**
     * @param $directory
     * @return array
     */
    public static function getFilesDirectory($directory)
    {
        if (is_dir($directory)) {
            $files = [];
            if ($handle = opendir($directory)) {
                while (false !== ($item = readdir($handle))) {
                    if (is_file("$directory/$item")) {
                        array_unshift($files, "$directory/$item");
                    } elseif (is_dir("$directory/$item") && ($item != ".") && ($item != "..")) {
                        $files = array_merge($files, HFile::getFilesDirectory("$directory/$item"));
                    }
                }
                closedir($handle);
            }
            return $files;
        }
        return [];
    }
}