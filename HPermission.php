<?php

namespace ltcorp\helpers;

use Yii;

/**
 * Class HPermission
 * @package ltcorp\helpers
 */
class HPermission
{
    const TYPE_NEW_USER = 0;

    const TYPE_DIRECTOR = 1;
    const TYPE_MANAGER = 2;
    const TYPE_DISPATCHER = 3;
    const TYPE_PARTNER = 100;
    const TYPE_ADMIN = 9999;

    const TYPE_FIRST_USER = -1;

    /**
     * @param $permissions
     * @return array
     *
     * TODO: optimize
     */
    public static function getClientRules($permissions)
    {
        $result = array();
        if ($permissions & !empty($permissions['groups'])) {
            foreach ($permissions['groups'] as $groupName => $group) {
                foreach ($group['actions'] as $actionName => $action) {
                    if (self::checkPermission($groupName, $actionName, $permissions)) {
                        $result[$groupName . ucfirst($actionName)] = true;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $permissions
     * @return array
     */
    public static function processForView(array $permissions)
    {
        $result = Yii::$app->params['permissions'];
        if (!empty($permissions['ip'])) {
            $result['ip'] = $permissions['ip'];
        }

        foreach ($result['groups'] as $groupName => &$group) {
            $modelName = '\app\models\\' . ucfirst($groupName);

            if (empty($permissions['groups'][$groupName])) {
                unset($result['groups'][$groupName]);
                continue;
            }

            if (!empty($permissions['groups'][$groupName]['ip'])) {
                $group['ip'] = $permissions['groups'][$groupName]['ip'];
            }

            foreach ($group['actions'] as $actionName => &$action) {
                if (empty($permissions['groups'][$groupName]['actions'][$actionName]['access'])) {
                    unset($result['groups'][$groupName]['actions'][$actionName]);
                    continue;
                }

                $action['ip'] = $permissions['groups'][$groupName]['actions'][$actionName]['ip'];
                $action['access'] = $permissions['groups'][$groupName]['actions'][$actionName]['access'];
            }

            if (!empty($group['properties'])) {
                foreach ($group['properties'] as $propertyName => &$property) {
                    $methodName = 'get' . ucfirst($propertyName);
                    $property = $modelName::$methodName();
                }
            }
        }
        return $result;
    }

    /**
     * @param $permissions
     * @return mixed
     */
    public static function processForSave($permissions)
    {
        foreach ($permissions['groups'] as &$group) {
            if (isset($group['name'])) {
                unset($group['name']);
            }
            foreach ($group['actions'] as &$action) {
                if (isset($action['name'])) {
                    unset($action['name']);
                }
            }
        }
        return $permissions;
    }

    /**
     * check access and ip for action route
     *
     * @param $permissions | array with permissions
     * @param $route | like "users.view" = %group%.%action%
     * @return bool
     */
    public static function checkPermission($groupName, $actionName = 'index', array $permissions = [])
    {
        if (!$permissions) {
            if (\Yii::$app->user->isGuest) {
                return false;
            }
            $permissions = Yii::$app->user->identity->permissions;
        }
        if (!empty($permissions['groups'][$groupName]['actions'][$actionName])) {
            $permission = $permissions['groups'][$groupName]['actions'][$actionName];
            $permission['ip'] = self::setPermittedIp($permissions, $groupName, $actionName);
            return $permission['access'] && in_array(getenv('REMOTE_ADDR'), $permission['ip']);
        }

        return false;
    }

    /**
     * @param array $permissions
     * @param $groupName
     * @param $propertyName
     * @return null
     */
    public static function getProperty(array $permissions, $groupName, $propertyName)
    {
        if (!empty($permissions['groups'][$groupName]['properties'][$propertyName])) {

        }

        return null;
    }

    /**
     * @param $groupName
     * @param $actionName
     * @return array
     */
    private static function setPermittedIp($permissions, $groupName, $actionName)
    {
        $group = $permissions['groups'][$groupName];
        $action = $group['actions'][$actionName];

        $ip = $action['ip'];
        if (!$ip) {
            $ip = $group['ip'];
        }
        if (!$ip) {
            $ip = Yii::$app->params['permissions']['ip'];
        }
        if (!$ip) {
            $ip = [getenv('REMOTE_ADDR')];
        }
        return $ip;
    }

    /**
     * load standard permissions for user type
     *
     * @param null $type
     * @return mixed
     */
    public static function loadPermissions($type = null)
    {
        $permissions = Yii::$app->params['permissions'];
        switch ($type) {
            case self::TYPE_DIRECTOR :
                $permissions = self::setDirectorPermissions($permissions);
                break;

            case self::TYPE_ADMIN :
                $permissions = self::setAdminPermissions($permissions);
                break;

            case self::TYPE_MANAGER :
                $permissions = self::setManagerPermissions($permissions);
                break;

            case self::TYPE_DISPATCHER :
                $permissions = self::setDispatcherPermissions($permissions);
                break;

            case self::TYPE_PARTNER :
                $permissions = self::setPartnerPermissions($permissions);
                break;

            case self::TYPE_NEW_USER :
                $permissions = self::setNewUserPermissions($permissions);
                break;

            case self::TYPE_FIRST_USER :
                $permissions = self::setFirstUserPermissions($permissions);
                break;

            default :
                break;
        }
        return $permissions;
    }

    /**
     * set permissions for first admin
     *
     * @param $permissions | array with permissions
     * @return mixed
     */
    public static function setFirstUserPermissions($permissions)
    {
        foreach ($permissions['groups'] as &$group) {
            foreach ($group['actions'] as &$action) {
                $action['access'] = true;
            }
        }
        return HPermission::processForSave($permissions);
    }

    /**
     * @param $permissions
     * @return mixed
     */
    public static function setNewUserPermissions($permissions)
    {
        foreach ($permissions['groups'] as &$group) {
            foreach ($group['actions'] as &$action) {
                $action['access'] = false;
            }
        }
        return HPermission::processForSave($permissions);
    }
}