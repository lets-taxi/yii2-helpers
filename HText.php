<?php
namespace ltcorp\helpers;

/**
 * Class HText
 * @package ltcorp\helpers
 */
class HText
{
    public static function transliterate_cyr($str) {
        $ru = 'а,б,в,г,д,е,ё,ж,з,и,к,л,м,н,о,п,р,с,т,у,ф,х,ц,ч,ш,щ,ь,ы,ъ,э,ю,я,А,Б,В,Г,Д,Е,Ё,Ж,З,И,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ч,Ш,Щ,Ь,Ы,Ъ,Э,Ю,Я';
        $en = 'a,b,v,g,d,e,e,zh,z,i,k,l,m,n,o,p,r,s,t,u,f,h,c,ch,sh,shch,,y,,e,yu,ya,A,B,V,G,D,E,E,ZH,Z,I,K,L,M,N,O,P,R,S,T,U,F,H,C,Ch,Sh,Shch,,Y,,E,Yu,Ya';
        return str_replace(explode(',', $ru), explode(',', $en), $str);
    }
}